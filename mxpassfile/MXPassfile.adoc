### The Matrix Token File

The file `.mxpass` in a user's home directory can contain matrix tokens or password to be used if the connection requires a token (and no tokens has been specified otherwise). On Microsoft Windows the file is named `%APPDATA%\matrix\mxpass.cfg` (where %APPDATA% refers to the Application Data subdirectory in the user's profile). Alternatively, the token file to use can be specified using the connection parameter mxpassfile or the environment variable `MXPASSFILE`.

This file should contain lines of the following format:

    matrixhost|localpart|domain|token

(You can add a reminder comment to the file by copying the line above and preceding it with #.) Each of the first three fields can be a literal value, or *, which matches anything. The token field from the first line that matches the current connection parameters will be used. (Therefore, put more-specific entries first when you are using wildcards.) If an entry needs to contain | or \, escape this character with \. 

The matrixhost field must be a '*' or the complete url to the matrix server, for example `\http://localhost:8008` or `\https://matrix.example.com`.

On Unix systems, the permissions on a token file must disallow any access to world or group; achieve this by a command such as `chmod 0600 ~/.mxpass`. If the permissions are less strict than this, the file will be ignored. On Microsoft Windows, it is assumed that the file is stored in a directory that is secure, so no special permissions check is made.
