package mxpassfile

import "runtime"

// DefaultPassfileName returns the default passfile file name
func DefaultPassfileName() string {
	switch runtime.GOOS {
	case "windows":
		return "mxpass.cfg"
	default:
		return "mxpass"
	}
}

// DefaultPassfileName returns the default hidden passfile file name
func DefaultPassfileHiddenName() string {
	switch runtime.GOOS {
	case "windows":
		return ".mxpass.cfg"
	default:
		return ".mxpass"
	}
}
