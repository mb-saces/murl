package mxpassfile

import (
	"bufio"
	"io"
	"os"
	"strings"
)

/*
This comment becomes rendered as asciidoc
tag::tag[]

if no tags are given, tags in file are ignored

if tags are given, all tags must be in the file

if empty tag is given (`murl -T "" …`), item line must have empty tag too (line ends with `|`)

end::tag[]
*/

// Entry represents a line in a MX passfile.
type entry struct {
	synapsehost string
	localpart   string
	domain      string
	token       string
	tags        map[string]struct{}
}

// Passfile is the in memory data structure representing a MX passfile.
type Passfile struct {
	entries []*entry
}

// ReadPassfile reads the file at path and parses it into a Passfile.
func readPassfile(path string) (*Passfile, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return ParsePassfile(f)
}

// ParsePassfile reads r and parses it into a Passfile.
func ParsePassfile(r io.Reader) (*Passfile, error) {
	passfile := &Passfile{}

	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		entry := parseLine(scanner.Text())
		if entry != nil {
			passfile.entries = append(passfile.entries, entry)
		}
	}

	return passfile, scanner.Err()
}

// parseLine parses a line into an *Entry. It returns nil on comment lines or
// any other unparsable line.
func parseLine(line string) *entry {
	const (
		tmpBackslash = "\r"
		tmpPipe      = "\n"
	)

	line = strings.TrimSpace(line)

	if strings.HasPrefix(line, "#") {
		return nil
	}

	line = strings.Replace(line, `\\`, tmpBackslash, -1)
	line = strings.Replace(line, `\|`, tmpPipe, -1)

	parts := strings.Split(line, "|")

	if len(parts) < 4 {
		return nil // too short
	}
	var tags map[string]struct{}

	if len(parts) > 4 {
		tags = make(map[string]struct{}, len(parts)-4)
		for _, p := range parts[4:] {
			tags[p] = struct{}{}
		}
	}

	// Unescape escaped colons and backslashes
	for i := range parts {
		parts[i] = strings.Replace(parts[i], tmpBackslash, `\`, -1)
		parts[i] = strings.Replace(parts[i], tmpPipe, `|`, -1)
	}

	return &entry{
		synapsehost: parts[0],
		localpart:   parts[1],
		domain:      parts[2],
		token:       parts[3],
		tags:        tags,
	}
}

// FindPassword finds the password for the provided synapsehost, localpart, and domain. An empty
// string will be returned if no match is found.
// This function ignores tags.
func (pf *Passfile) FindPassword(synapsehost, localpart, domain string) string {
	return pf.FindPasswordTags(synapsehost, localpart, domain, []string{})
}

// FindPasswordTagsOnly finds the password for the provided tags. An empty
// string will be returned if no match is found.
// This function uses only tags.
func (pf *Passfile) FindPasswordTagsOnly(tags ...string) string {
	return pf.FindPasswordTags("*", "*", "*", tags)
}

// FindPasswordTags finds the password for the provided synapsehost, localpart, domain, and any given tag. An empty
// string will be returned if no match is found.
// All given aditional tags must fit.
func (pf *Passfile) FindPasswordTags(synapsehost, localpart, domain string, tags []string) string {
	for _, e := range pf.entries {
		if len(tags) > 0 && !checkTags(e.tags, tags) {
			continue
		}
		if (synapsehost == "*" || e.synapsehost == synapsehost) &&
			(localpart == "*" || e.localpart == localpart) &&
			(domain == "*" || e.domain == domain) {
			return e.token
		}
	}
	return ""
}

// FindHostPassword finds the host and password for the provided localpart and domain. Empty
// strings will be returned if no match is found.
// This function ignores tags.
func (pf *Passfile) FindHostPassword(localpart, domain string) (string, string) {
	return pf.FindHostPasswordTags(localpart, domain, []string{})
}

// FindHostPasswordTagsOnly finds the host and password for the provided tags. Empty
// strings will be returned if no match is found.
// This function uses only tags.
func (pf *Passfile) FindHostPasswordTagsOnly(tags ...string) (string, string) {
	return pf.FindHostPasswordTags("*", "*", tags)
}

// FindHostPasswordTags finds the host and password for the provided localpart, domain, and any given tag. Empty
// strings will be returned if no match is found.
// All given aditional tags must fit.
func (pf *Passfile) FindHostPasswordTags(localpart, domain string, tags []string) (string, string) {
	for _, e := range pf.entries {
		if len(tags) > 0 && !checkTags(e.tags, tags) {
			continue
		}
		if (localpart == "*" || e.localpart == localpart) &&
			(domain == "*" || e.domain == domain) {
			return e.synapsehost, e.token
		}
	}
	return "", ""
}

// checkTags returns true if source contains all tags from test
func checkTags(source map[string]struct{}, test []string) bool {
	for _, t := range test {
		if _, ok := source[t]; !ok {
			return false
		}
	}
	return true
}
