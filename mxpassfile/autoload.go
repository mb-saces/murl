package mxpassfile

import (
	"os"
)

/*
This comment becomes rendered as asciidoc
tag::loc[]

MXPassfile is searched at the following locations, the first found is taken

- Current working directory
- User config directory
- User home directory

MXPassfile default locations:
[cells="1,1,1,1,1", frame=none, grid=none]
|===
| |Unix|Plan9|Mac/iOS|Windows

|CurrentDir |.mxpass |.mxpass |.mxpass |.mxpass.cfg

|UserHome |$HOME/.mxpass |$home/.mxpass |$HOME/.mxpass |%userprofile%\.mxpass.cfg

|UserConfig
|$XDG_CONFIG_HOME/matrix/mxpass +
or +
$HOME/.config/matrix/mxpass
|$home/lib/matrix/mxpass
|/Library/Application&#160;Support/matrix/mxpass
|%AppData%\matrix\mxpass.cfg


|===

end::loc[]
*/

// FindMXPassfile searches the default locations for mxpassfiles and returns the first hit
func FindMXPassfile() (path string, err error) {
	path, exist, err := ConfigFileExist(os.Getwd, DefaultPassfileHiddenName())
	if exist || err != nil {
		return
	}

	path, exist, err = ConfigFileExist(os.UserConfigDir, "matrix", DefaultPassfileName())
	if exist || err != nil {
		return
	}

	path, exist, err = ConfigFileExist(os.UserHomeDir, DefaultPassfileHiddenName())
	if exist || err != nil {
		return
	}
	return "", nil
}

// Autoload loads the given mxpass file. If path is empty, the default locations are searched.
func Autoload(path string) (pf *Passfile, err error) {
	if path == "" {
		// MXPASSFILE was not set, check default locations
		path, err = FindMXPassfile()
		if err != nil {
			return
		}
		if path == "" {
			// not found
			return
		}
	}
	pf, err = ReadPassfile(path)
	return
}
