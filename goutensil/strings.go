package goutensil

// Star returns "*" if s is empty, otherwise s
func Star(s string) string {
	if s == "" {
		return "*"
	}
	return s
}
