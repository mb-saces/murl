module gitlab.com/mb-saces/murl

go 1.19

require (
	maunium.net/go/mautrix v0.12.4
	github.com/fatih/structtag v1.2.0
	github.com/jessevdk/go-flags v1.5.0
)

require (
	github.com/tidwall/gjson v1.14.4 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.0 // indirect
	github.com/tidwall/sjson v1.2.5 // indirect
	golang.org/x/crypto v0.4.0 // indirect
	golang.org/x/net v0.4.0 // indirect
	golang.org/x/sys v0.3.0 // indirect
)
