// THIS FILE IS GENERATEd!1! - DO NOT EDIT!
package main

type ConfigMatrixClient struct {
	HSUrl     string `env:"MXHSURL" short:"h" long:"homeserver-url" description:"Matrix homeserver url"`
	Token     string `env:"MXTOKEN" short:"t" long:"token" description:"Matrix access token"`
	UserID    string `env:"MXID" short:"u" long:"mxid" description:"Matrix user id"`
	Localpart string `env:"MXLOCALPART" short:"l" long:"localpart" description:"Matrix user id localpart"`
	Domain    string `env:"MXDOMAIN" short:"d" long:"domain" description:"Matrix user id domain"`
	Passfile  string `env:"MXPASSFILE" short:"p" long:"passfile" description:"Matrix user passfile"`
}
