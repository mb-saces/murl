//go:build ignore

package main

import (
	"go/format"
	"os"
	"strings"

	"gitlab.com/mb-saces/murl/confutils"
	"gitlab.com/mb-saces/murl/goutensil"
)

func main() {

	var conf confutils.GeneratorConf

	conf = confutils.GeneratorConf{
		TypeName: "ConfigMatrixClient",
		Sources: []confutils.GeneratorConfSource{
			{
				ConfigTemplate: confutils.ConfigMatrixClient{},
				Items: []confutils.GeneratorConfItem{
					{FieldName: "HSUrl"},
					{FieldName: "Token"},
					{FieldName: "UserID"},
					{FieldName: "Localpart"},
					{FieldName: "Domain"},
					{FieldName: "Passfile"},
				},
			},
		},
	}
	sb := &strings.Builder{}

	confutils.SimpleGenerate(sb, "main", conf)

	src := sb.String()
	//	fmt.Println(src)
	fsrc, err := format.Source([]byte(src))
	goutensil.MaybePanic(err)
	err = os.WriteFile("config.go", fsrc, 0o644)
	goutensil.MaybePanic(err)

}
