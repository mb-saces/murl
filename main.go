package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"runtime"
	"strings"
	"time"

	"github.com/jessevdk/go-flags"
	"gitlab.com/mb-saces/murl/goutensil"
	"gitlab.com/mb-saces/murl/mxpassfile"
)

//go:generate go run generate.go

// Make a variable for the version which will be set at build time.
var version = "unknown"

// commandline parser and envirionment variables are configured via struct tags
type MurlConfig struct {
	ConfigMatrixClient
	Verbose    []bool   `short:"v" long:"verbose" description:"Verbose output"`
	Version    bool     `short:"V" long:"version" description:"show version info"`
	Help       bool     `long:"help" description:"show help"`
	Method     string   `short:"X" long:"method" description:"HTTP method"`
	Header     []string `short:"H" long:"header" description:"HTTP header"`
	AuthHeader bool     `short:"A" long:"auth-header" description:"add HTTP auth header"`
	JSONHeader bool     `short:"J" long:"json-header" description:"add header for content type json"`
	Output     string   `short:"o" long:"output" default:"-" description:"write output to"`
	Tags       []string `env:"MXTAGS" env-delim:"," short:"T" long:"tag" description:"tags for mxpassfile"`
	Payload    string   `short:"f" long:"payload" description:"read payload from file"`
	MaxRetry   int      `short:"m" long:"max-retry" default:"5" description:"max retries on 429 errors (rate limit)"`
	Positional struct {
		URL     string   `required:"yes" description:"url to fetch/put"`
		Payload []string `description:"data to send"`
	} `positional-args:"yes"`
}

var mCfg MurlConfig

type RateLimitResponse struct {
	ErrorCode    string `json:"errcode"`
	Error        string `json:"error,omitempty"`
	RetryAfterMS int    `json:"retry_after_ms,omitempty"`
}

func printReqHeader(r *http.Request) {
	fmt.Println()
	for name, values := range r.Header {
		// Loop over all values for the name.
		for _, value := range values {
			fmt.Fprintf(os.Stderr, "%s: %s\n", name, value)
		}
	}
}

func printResHeader(r *http.Response) {
	fmt.Println()
	for name, values := range r.Header {
		// Loop over all values for the name.
		for _, value := range values {
			fmt.Fprintf(os.Stderr, "%s: %s\n", name, value)
		}
	}
}

func main() {
	// create commandline parser
	var parser = flags.NewParser(&mCfg, flags.PassDoubleDash)

	// the parser is configured to treat any additional arguments as payload,
	// so the remainig part will be allways empty
	_, err := parser.Parse()

	// first sort out requests for help
	if mCfg.Help {
		parser.WriteHelp(os.Stderr)
		os.Exit(0)
	}

	// Write out the version
	if mCfg.Version {
		fmt.Fprintf(os.Stderr, "[M]Url version %s on %s/%s\n", version, runtime.GOOS, runtime.GOARCH)
		os.Exit(0)
	}

	// some error. print help
	if err != nil {
		fmt.Fprintf(os.Stderr, "Parser error: %v\n", err)
		parser.WriteHelp(os.Stderr)
		os.Exit(1)
	}

	// unroll & check verbose level. 0, 1, 3 are valid values
	var verboselevel int = len(mCfg.Verbose)

	switch verboselevel {
	case 0: // do nothing (aka 'Shut up!')
	case 1, 3:
		fmt.Fprintf(os.Stderr, "[M]Url version %s on %s/%s\n", version, runtime.GOOS, runtime.GOARCH)
		fmt.Fprintf(os.Stderr, "Verbose Level %d\n", verboselevel)
	default:
		goutensil.MaybeExit(fmt.Errorf("invalid verbose level '%d'. Valid values are 0, 1, 3", verboselevel))
	}

	// check for localpart & domain vs mxid
	if (mCfg.Domain != "" || mCfg.Localpart != "") && mCfg.UserID != "" {
		goutensil.MaybeExit(errors.New("either use domain & localpart, or mxid, but not booth"))
	}

	// check mxtoken & mxpassfile
	if mCfg.Passfile != "" && mCfg.Token != "" {
		goutensil.MaybeExit(errors.New("passfile and token are mutually exclusive"))
	}

	// check payload. argument and -f option are mutually exclusive.
	if len(mCfg.Positional.Payload) != 0 && mCfg.Payload != "" {
		goutensil.MaybeExit(errors.New("payload as argument and as file option are mutually exclusive"))
	}

	// check payload file for existence
	if mCfg.Payload != "" {
		exist, err := goutensil.FileExists(mCfg.Payload)
		goutensil.MaybeExit(err)
		if !exist {
			goutensil.MaybeExit(errors.New("payload file does not exist"))
		}
		if verboselevel > 0 {
			fmt.Fprintf(os.Stderr, "INFO: Using file as payload: %s\n", mCfg.Payload)
		}
	}

	// autoload mxpassfile. if pf and err is nil, no mxpassfile was found
	pf, err := mxpassfile.Autoload(mCfg.Passfile)
	goutensil.MaybeExit(err)

	if pf == nil && verboselevel > 0 {
		fmt.Fprintln(os.Stderr, "No mxpassfile found.")
	}

	// get the url part from arguments and anylyse it
	u, err := url.Parse(mCfg.Positional.URL)
	goutensil.MaybeExit(err)

	// if url argument does not contain a host,
	// try to guess it from passfile item or options
	if u.Host == "" {
		var hs, t string
		if pf != nil {
			hs, t = pf.FindHostPasswordTags(goutensil.Star(mCfg.Localpart), goutensil.Star(mCfg.Domain), mCfg.Tags)
		}
		if hs != "" {
			// got a host from passfile,
			// store the token so we do not need to lookup it again
			mCfg.Token = t
		} else {
			// no host from passfile, try to guess from options
			hs = mCfg.HSUrl
		}
		h, err := url.Parse(hs)
		goutensil.MaybeExit(err)
		u = h.ResolveReference(u)
		goutensil.MaybeExit(err)
		if verboselevel > 0 {
			fmt.Fprintf(os.Stderr, "Use guessed URL: %s\n", u)
		}
	}

	tr := &http.Transport{
		MaxIdleConns:       10,
		IdleConnTimeout:    30 * time.Second,
		DisableCompression: true,
	}
	client := &http.Client{Transport: tr}

	if mCfg.Method == "" {
		mCfg.Method = "GET"
	}

	// retry counter must be declared before the loop
	var retryCounter int = 0

RetryHack:

	var payload io.Reader = nil
	if mCfg.Payload != "" {
		f, err := os.Open(mCfg.Payload)
		goutensil.MaybePanic(err)
		defer f.Close()
		payload = f
	} else if len(mCfg.Positional.Payload) == 1 {
		if mCfg.Positional.Payload[0] == "-" {
			// no retry if payload is read from stdin
			mCfg.MaxRetry = 0
			payload = os.Stdin
		} else {
			payload = strings.NewReader(mCfg.Positional.Payload[0])
		}
	} else if len(mCfg.Positional.Payload) > 1 {
		var readerse []io.Reader
		for _, pl := range mCfg.Positional.Payload {
			readerse = append(readerse, strings.NewReader(pl))
		}
		payload = io.MultiReader(readerse...)
	}

	r, err := http.NewRequest(mCfg.Method, u.String(), payload)
	goutensil.MaybePanic(err)

	r.Header.Set("User-Agent", "murl/42")
	if mCfg.AuthHeader {
		if mCfg.Token == "" {
			mCfg.Token = pf.FindPasswordTags(goutensil.Star(mCfg.HSUrl), goutensil.Star(mCfg.Localpart), goutensil.Star(mCfg.Domain), mCfg.Tags)
		}
		r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", mCfg.Token))
	}
	if mCfg.JSONHeader {
		r.Header.Set("Content-Type", "application/json; charset=UTF-8")
	}

	if verboselevel > 2 {
		printReqHeader(r)
	}

	resp, err := client.Do(r)
	goutensil.MaybeExit(err)

	if resp.StatusCode == 429 && retryCounter < mCfg.MaxRetry {
		var buf bytes.Buffer
		_, err = io.Copy(&buf, resp.Body)
		goutensil.MaybeExit(err)

		var rlr RateLimitResponse

		err = json.Unmarshal(buf.Bytes(), &rlr)
		goutensil.MaybeExit(err)

		if rlr.RetryAfterMS <= 0 {
			goutensil.MaybeExit(errors.New("retry_after_ms was not set or is < 0"))
		}

		retryCounter += 1
		if verboselevel > 0 {
			fmt.Fprintf(os.Stderr, "Rate limit. Retry in %d ms. This was attempt № %d.\n", rlr.RetryAfterMS, retryCounter)
		}
		time.Sleep(time.Duration(rlr.RetryAfterMS * int(time.Millisecond)))

		goto RetryHack
	}

	if verboselevel > 0 {
		printResHeader(resp)
	} else {
		switch resp.StatusCode {
		case 200, 201, 204, 404: // do nothing
		default:
			printResHeader(resp)
		}
	}

	var out io.Writer
	if mCfg.Output == "-" {
		out = os.Stdout
	} else {
		f, err := os.Create(mCfg.Output)
		goutensil.MaybeExit(err)
		if verboselevel > 2 {
			out = io.MultiWriter(os.Stdout, f)
		} else {
			out = f
		}
	}
	_, err = io.Copy(out, resp.Body)
	goutensil.MaybeExit(err)

	switch resp.StatusCode {
	case 200, 201, 204:
		os.Exit(0)
	case 401:
		os.Exit(23)
	case 429:
		os.Exit(9)
	case 404:
		os.Exit(4)
	default:
		os.Exit(42)
	}
	// Bye.
}
