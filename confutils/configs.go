package confutils

type ConfigFileConfig struct {
	ConfigFile     string `env:"CONFIG_FILE" description:"path to config file"`
	ConfigFileName string `env:"CONFIG_FILE_NAME" description:"name of config file"`
	ConfigFileDir  string `env:"CONFIG_FILE_DIR" description:"directory to look up config file"`
	ConfigType     string `env:"CONFIG_FILE_TYPE" default:"yaml" description:"The type of the configuration file"`
}

type ConfigMatrixClient struct {
	HSUrl     string `env:"MXHSURL" short:"h" long:"homeserver-url" description:"Matrix homeserver url"`
	Token     string `env:"MXTOKEN" short:"t" long:"token" description:"Matrix access token"`
	UserID    string `env:"MXID" short:"u" long:"mxid" description:"Matrix user id"`
	Password  string `env:"MXPASSWORD" short:"W" long:"password" description:"Matrix user password"`
	Localpart string `env:"MXLOCALPART" short:"l" long:"localpart" description:"Matrix user id localpart"`
	Domain    string `env:"MXDOMAIN" short:"d" long:"domain" description:"Matrix user id domain"`
	Passfile  string `env:"MXPASSFILE" short:"p" long:"passfile" description:"Matrix user passfile"`
}

type ConfigPostgres struct {
	Port     string `env:"PGPORT" short:"p" long:"port" env-default:"5432" description:"postgres database port"`
	Host     string `env:"PGHOST" short:"h" long:"host" env-default:"localhost" description:"postgres database host"`
	DBName   string `env:"PGDATABASE" short:"d" long:"dbname" description:"postgres database"`
	User     string `env:"PGUSER" short:"U" long:"user" description:"postgres database user"`
	Password string `env:"PGPASSWORD" description:"postgres database password"`
	Passfile string `env:"PGPASSFILE" description:"postgres password file" no-flag:"1"`
}
