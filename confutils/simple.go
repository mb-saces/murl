package confutils

import (
	"fmt"
	"io"
	"reflect"
)

type GeneratorConf struct {
	TypeName string
	VarName  string
	Sources  []GeneratorConfSource
}

type GeneratorConfSource struct {
	ConfigTemplate interface{}
	Items          []GeneratorConfItem
}

type GeneratorConfItem struct {
	FieldName string
	//	Targets     []string
	ShortOption string
	LongOption  string
	Tag         string
}

func SimpleGenerate(sw io.StringWriter, packagename string, gconf GeneratorConf) {

	sw.WriteString("// THIS FILE IS GENERATEd!1! - DO NOT EDIT!\n")
	sw.WriteString("package ")
	sw.WriteString(packagename)
	sw.WriteString("\n\n")

	sw.WriteString("type ")
	sw.WriteString(gconf.TypeName)
	sw.WriteString(" struct {\n")

	for _, conf := range gconf.Sources {
		v := reflect.TypeOf(conf.ConfigTemplate)
		for _, item := range conf.Items {
			f, ok := v.FieldByName(item.FieldName)
			if !ok {
				panic(fmt.Sprintf("No such field: %s\n", item))
			}
			sw.WriteString(f.Name)
			sw.WriteString(" ")
			sw.WriteString(f.Type.Name())
			sw.WriteString(" `")
			sw.WriteString(string(f.Tag))
			sw.WriteString("`")
			sw.WriteString("\n")
		}
	}
	sw.WriteString("}\n\n")
}
